#! /usr/bin/env ruby
require './bot_version.rb'
require 'sinatra/base'
require 'json'
require 'cinch'
require 'cinch/plugins/basic_ctcp'
require 'cinch/plugins/identify'
require 'ostruct'
require 'recursive-open-struct'
require 'yaml'
require 'unirest'
require 'base64'
require 'active_support/all'
Thread.abort_on_exception = true

# @note Load the plugins
require './lib/chancontrol.rb'
require './lib/admin.rb'
require "./lib/logger.rb"

# @note Load the parsers
require './lib/gitlab.rb'
require './lib/github.rb'

# Cinch

$cfg = YAML.load_file(`echo ~/.gitall-rc.yml`.chomp!)
$bots = Hash.new
$threads = Array.new

# For the sake of this example, we'll create an ORM-like model and give it an
# authenticate method, that checks if a given password matches.
class User < Struct.new :nickname, :password, :type
  def authenticate(pass)
    password == pass
  end
end

# Simulate a database.
$users = []
@users = YAML.load_file(`echo ~/.gitall-rc.yml`.chomp!)['users']
@users.each do |user, hash|
  password = hash['password']
  role = hash['role']
  $users << User.new(user, password, role)
end

$cfg['networks'].each do |name, ncfg|
  bot = Cinch::Bot.new do
    configure do |c|
      c.server = ncfg.fetch('server')
      c.port = ncfg.fetch('port')
      c.nick = ncfg.fetch('nickname')
      c.user = ncfg.fetch('username')
      c.realname = ncfg.fetch('realname')
      c.plugins.plugins << Cinch::Plugins::Identify
      identify_with = ncfg.fetch('identify-with')
      case identify_with
      when 'nickserv'
        begin
          c.plugins.options[Cinch::Plugins::Identify] = {
            :username => ncfg.fetch('sasl-username'),
            :password => ncfg.fetch('sasl-password'),
            :type     => :nickserv,
          }
        rescue KeyError
        end
      when 'sasl'
        begin
          c.sasl.username = ncfg.fetch('sasl-username')
          c.sasl.password = ncfg.fetch('sasl-password')
        rescue KeyError
        end
      when 'cert'
        begin
          c.ssl.client_cert = ncfg.fetch('certfp')
        rescue KeyError
        end
      end
      c.channels = ncfg.fetch('channels')
      c.ssl.use = ncfg.fetch('ssl')
      c.ssl.verify = ncfg.fetch('sslverify')
      c.messages_per_second = ncfg.fetch('mps')

      # Global configuration. This means that all plugins / matchers that
      # implement authentication make use of the :login strategy, with a user
      # level of :users.
      c.authentication          = Cinch::Configuration::Authentication.new
      c.authentication.strategy = :login
      c.authentication.level    = :users

      # The UserLogin plugin will call this lambda when a user runs !register.
      c.authentication.registration = lambda { |nickname, password|
        # If you were using an ORM, you'd do something like
        # `User.create(:nickname => nickname, :password => password)` here.
        return false if $users.one? { |user| user.nickname == nickname }
        $users << User.new(nickname, password, 'user')
      }

      # The UserLogin plugin will call this lambda when a user runs !login. Note:
      # the class it returns must respond to #authenticate with 1 argument (the
      # password the user tries to login with).
      c.authentication.fetch_user = lambda { |nickname|
        # If you were using an ORM, you'd probably do something like
        # `User.first(:nickname => nickname)` here.
        $users.find { |user| user.nickname == nickname }
      }

      # The Authentication mixin will call these lambdas to check if a user is
      # allowed to run a command.
      c.authentication.users  = lambda { |user| user.type == 'user' }
      c.authentication.admins = lambda { |user| user.type == 'admin' }
      c.plugins.plugins << Cinch::Plugins::UserLogin
      c.plugins.plugins << Cinch::Plugins::BasicCTCP
      c.plugins.options[Cinch::Plugins::BasicCTCP][:replies] = {
          version: Version::Bot.version,
          source:  'https://gitlab.com/gitall/gitall'
      }
      c.plugins.plugins << ChanControl
      c.plugins.options[ChanControl][:authentication_level] = :admins
      c.plugins.plugins << Admin
      c.plugins.options[Admin][:authentication_level] = :admins
      c.plugins.plugins << Cinch::Plugins::UserList
      c.plugins.options[Cinch::Plugins::UserList][:authentication_level] = :admins
    end
  end
  bot.loggers.clear
  bot.loggers << GitLogger.new(name, File.open("log/gitall-#{name}.log", "a"))
  bot.loggers << GitLogger.new(name, STDOUT)
  bot.loggers.level = :debug
  $bots[name] = bot
end
$bots.each do |key, bot|
  puts "Starting IRC connection for #{key}..."
  $threads << Thread.new { bot.start }
end

# Shortener

def shorten(url)

  api = 'https://api.rebrandly.com/v1/links'
  params = {
    :apikey => $cfg['apikey'],
    :destination => url,
    :description => url,
    :title       => url,
    :domain => {
        :id => 'f266d3cddc0347aca001395249c067f6',
        :ref => '/domains/f266d3cddc0347aca001395249c067f6'
    }
  }
  response = Unirest.post api,
              headers: {'content-type': 'application/json'},
              parameters:params.to_json

  'http://' + response.body['shortUrl']
end

# Hook

# *getFormat*
#
# Returns the message format for the received
#   hook type.
# @param kind [String] event type
# @param json [JSON] json hash

class MyApp < Sinatra::Base
  # ... app code here ...
  set :port, 8008
  set :bind, '127.0.0.1'
  set :environment, 'production'
  set :threaded, true
  post '/hook/?' do
    request.body.rewind
    json = JSON.load(request.body.read)
    j = RecursiveOpenStruct.new(json)
    repo = ''
    if request.env.key? 'HTTP_X_GITLAB_TOKEN'
      repo = j.project.path_with_namespace
      resp = GitLabParser.parse json
      phash = $cfg['projects'][repo]
      if phash['token'] == request.env['HTTP_X_GITLAB_TOKEN']
        channels = phash['channels']
        channels.each do |channet|
          channel = channet.split(',')[0]
          net = channet.split(',')[1]
          resp.each do |n|
            $bots[net].Channel(channel).send("#{n}")
          end
        end
      end
    elsif request.env.key? 'HTTP_X_HUB_SIGNATURE'
      if !j.repository.full_name.nil?
        repo = j.repository.full_name
      elsif !j.organization.login.nil?
        repo = j.organization.login
      else
      end
      # phash includes orgs and repos
      phash = $cfg['projects'][repo]
      token = phash['token']
      sent_token = request.env['HTTP_X_HUB_SIGNATURE']
      signature = "sha1=#{OpenSSL::HMAC.hexdigest('sha1', token, payload.strip).chomp}"
      resp = GitHubParser.parse json, request.env['HTTP_X_GITHUB_EVENT']
      if signature.to_s == sent_token.to_s
        channels = phash['channels']
        channels.each {
          |channet|
          channel = channet.split(',')[0]
          network = channet.split(',')[1]
          resp.each {
            |n|
            $bots[network].Channel(channel).send("#{n}")
          }
        }
      end
    elsif request.env.key?('HTTP_X_EVENT_KEY')
      # BitBucket's Webhooks
      # Requires HTTPS and does not
      # implement a secret unless set into
      # the url.
      status 404
      body 'bitbucket not implemented'
    else
      return [404, "I can't help with that"]
    end
  end
end
# start the server if ruby file executed directly
$threads << Thread.new {
  MyApp.run! if __FILE__ == $PROGRAM_NAME
}
$threads.each(&:join)
