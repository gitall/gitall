module Version
  class Bot
    def self.version
      version_list = File.open('version.txt', 'r').read.chomp!.split('.')
      return "gitall bot v#{version_list.join('.')}"
    end
  end
end
