## GitAll

This is a Ruby+Sinatra mini-app that catches Web Based Git Services' webhooks and returns them to an IRC channel.

To use, you must first install the following gems,

```gem install recursive-open-struct sinatra cinch cinch-identify cinch-basic_ctcp activesupport```

If you want to be entered into the already running hook, please use [this](https://electrocode.net/gitall/request-hook-usage/)
