require 'cinch'
require 'cinch/extensions/authentication'
require 'yaml'
require 'recursive-open-struct'

# @class Admin Plugin
class Admin
  include Cinch::Plugin
  include Cinch::Extensions::Authentication
  match /quit/, method: :do_Quit, group: :quit
  match /quit (.*)/, method: :do_Quit, group: :quit

  match /join/, method: :do_Join, group: :join
  match /join (.*)/, method: :do_Join, group: :join

  def do_Quit(m, msg = nil)
    return unless authenticated? m
    if msg.nil?
      $bots.each do |net, bot|
        m.reply 'Quitting!'
        bot.quit('QUIT received!')
      end
    else
      $bots.each do |net, bot|
        m.reply 'Quitting!'
        bot.quit(msg)
      end
    end
    $threads.each(&:exit)
  end
  def do_Join(m, channels = nil)
  	return unless authenticated? m
  	if channels.nil?
  	  m.reply 'Syntax: join #channel[,#channel2,#channel3]'
  	else
  	  chans = channels.split(',')
  	  chans.each do |chan|
  	    Channel(chan).join
  	  end
   	end
  end
  def do_Part(m, channels = nil, reasons = nil)
    return unless authenticated? m
    if channels.nil? && reasons.nil?
      m.reply 'Syntax: part #channel1[,#channel2,#channel3] [reason1[,reason2,reason3]]'
    else
      m.reply 'Not implemented yet'
    end
  end
end
