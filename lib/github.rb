# GitHub Event Parsing
# noinspection RubyUnusedLocalVariable
class GitHubParser
  # @param [Object] json json object
  # @param [Object] event the github event
  def self.parse(json, event)
    j = RecursiveOpenStruct.new(json)
    response = []
    case event
    when 'ping'
      zen = j.zen
      id = j.hook_id
      response << "Hook #{id} [PING] '#{zen}'"
    when 'issue_comment'
    when 'member'
    when 'membership'
    when 'milestone'
    when 'label'
#        "action":"created",
#  			"label":{
#    		"url":"https://api.github.com/repos/baxterandthehackers/public-repo/labels/blocked",
#    		"name":"blocked",
#    		"color":"ff0000"
#  		},
      action = j.action
      l = j.label
      repo = j.repository.full_name
      user = j.sender.login
      lurl = l.url
      lname = l.name
      case action
      when 'created', 'deleted'
        response << "[#{repo}] #{user} #{action} label #{lname} <#{lurl}>"
      when 'edited'
        old = l.changes.name.from
        new = lname
        if old != new
          response << "[#{repo}] #{user} #{action} Old: #{old} New: #{new}"
        end
      end
    when 'team'
    when 'team_add'
    when 'fork'
      f = RecursiveOpenStruct.new(j.forkee)
      user = j.sender.login
      fork = f.full_name
      repo = j.repository.full_name
      response << "[#{repo}] #{user} forked the repository to #{fork}"
    when 'pull_request'
      p = RecursiveOpenStruct.new(j.pull_request)
      action = j.action
      case action
      when 'opened'
      when 'closed'
      end
    when 'pull_request_review'
      action = j.action
      case action
      when 'created'
      when 'deleted'
      end
    when 'pull_request_review_comment'
      c = RecursiveOpenStruct.new(j.comment)
      action = j.action
      case action
      when 'created'
      when 'deleted'
      when 'edited'
      end
    when 'commit_comment'
    when 'gollum'
      p = j.pages
      action = j.action
    when 'create'
    when 'delete'
    when 'organization'
      action = j.action
      case action
      when 'member_add'
      when 'member_del'
      end
    when 'repository'
      r = RecursiveOpenStruct.new(j.repository)
      action = j.action
      repo = r.full_name
      user = j.sender.login
      case action
      when 'created'
        response << "#{user} created repository #{repo}"
      when 'deleted'
        # XXX: Only triggers on Organization webhooks
        response << "#{user} deleted #{r.full_name}"
      end
    when 'release'
      r = RecursiveOpenStruct.new(j.release)
      tag = r.tag_name
      action = j.action
      repo = j.repository.full_name
      case action
      when 'published'
        response << "[#{repo}] released #{tag}!"
      else
        response << "[#{repo}] #{action} something!"
        response << "**\u000304This is a fallback for when new actions are added to the 'ReleaseEvent'**"
        response << 'Please submit an issue at https://github.com/IotaSpencer/gitall'
        response << ' or join #gitall on the freenode or ElectroCode IRC networks.'
      end
    when 'watch'
      # XXX: possibly add user's profile url if option set
      response << "[#{repo}] #{user} starred the repository!"
    when 'issue'
      repo = j.repository.full_name
      url = shorten(j.issue.url)
      action = j.action
      id = j.issue.number
      title = j.issue.title
      user = j.issue.user.name
      # assigned", "unassigned", "labeled", "unlabeled", "opened", "edited",
      # "milestoned", "demilestoned", "closed", or "reopened".
      case action
      when 'assigned'
        # do stuff ...
      when 'unassigned'
        # do stuff ...
      when 'labeled'
        label = j.label.name
        response << "[#{repo}] #{user} added #{label}"
      when 'unlabeled'
        # do stuff ...
      when 'milestoned'
        response << "[#{repo}] #{user} milestoned issue ##{id} "
      when 'demilestoned'
        # do stuff ...
      when 'reopened'
        response << "[#{repo}] #{user} reopened ##{id}"
        
      when 'opened'
        response << "[#{repo}] #{user} opened an issue ##{id} - #{title} <#{url}>"
      when 'closed'
        response << "[#{repo}] #{user} closed issue ##{id} - #{title} <#{url}>"
      when 'edited'
        response << "[#{repo}] #{user} edited issue ##{id} - #{title} <#{url}>"
      end
    when 'push'
      repo = j.repository.full_name
      branch = j.ref.split('/')[-1]
      commits = j.commits
      added = 0
      removed = 0
      modified = 0
      commits.each do |h|
        added    += h['added'].length
        removed  += h['removed'].length
        modified += h['modified'].length
      end
      pusher = j.sender.name
      commit_count = j['size']
      compare_url = shorten(j.compare)
      response << "[#{repo}] #{pusher} pushed #{commit_count} commit(s) [+#{added}/-#{removed}/±#{modified}] to [#{branch}] <#{compare_url}>"
      if commits.length > 3
        coms = commits[0..2]
        coms.each do |n|
          id = n.dig('id')
          id = id[0...7]
          msg = n.dig('message').lines[0]
          author = n.dig 'committer', 'name'
          response << "#{author} — #{msg} [#{id[0...7]}]"
        end
        response << "and #{j.size - 3} commits..."
      else
        commits.each do |n|
          id = n.dig('id')
          id = id[0...7]
          msg = n.dig('message').lines[0]
          author = n.dig 'committer', 'name'
          response << "#{author} — #{msg} [#{id}]"
        end
      end
    else 'format not implemented'
    end
    response
  end
end
