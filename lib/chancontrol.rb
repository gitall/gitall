require 'cinch'
require 'cinch/extensions/authentication'
require 'yaml'
require 'recursive-open-struct'
require 'strgen'
# @note ChanControl Plugin

class ChanControl
  include Cinch::Plugin
  include Cinch::Extensions::Authentication

  # Write to the config
  # @param [Hash] msg the data to write
  def toFile(msg)
    data = msg
    File.open(`echo ~/.gitall-rc.yml`.chomp, 'w') {|f| f.write(data.to_yaml) }
  end

  # Load the config
  # @return [Hash] load the current config
  def deFile
    begin
      parsed = YAML.load(File.open(`echo ~/.gitall-rc.yml`.chomp, 'r'))
    rescue ArgumentError => e
      puts "Could not parse YAML: #{e.message}"
    end
    parsed
  end

  match /padd/, method: :padd, group: :padd
  match /padd (\S+)/, method: :padd, group: :padd

  match /list/, method: :listchans, group: :listchans
  match /list (\S+)/, method: :listchans, group: :listchans

  match /token/, method: :getToken, group: :token
  match /token ([0-9]+)/, method: :getToken, group: :token

  def padd(m, namespace = nil, channels)
    return unless authenticated? m
    if namespace.nil?
      m.reply 'Syntax: padd {OWNER/PROJECT, ORG, GROUP, USER} #channel,network[,#channel2,network,#channel3,network2]'
    end
  end

  # @param [Message] m message object
  # @param [Integer] length token length
  # @return [String] Token
  def getToken(m, length = nil)
    return unless authenticated? m
    token = Strgen.generate do |c|
      c.length  = length.nil? ? 30 : length
      c.alpha   = true
      c.numbers = true
      c.repeat  = false
      c.symbols = false
      c.exclude = %w[1 i I l L 0 o O]
    end
    m.user.send "Token: #{token}"
  end

  # @param [Object] m Object
  # @param [String] channel name of channel
  def self.joined(channel)
    if Channel(channel)
      return true
    else
      return false
    end
  end
  def listchans(m, project = nil)
    return unless authenticated? m
    project_hash = deFile['projects'][project]
    m.reply "Name: #{project} Host: #{project_hash['host']} Channels: #{project_hash['channels'].join(' \xB7 ')}"
  end
end
